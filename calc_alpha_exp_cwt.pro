PRO CALC_ALPHA_EXP_CWT
  ; THIS ROUTINE CALCULATES THE SPATIAL SCALING EXPONENT -- THE POWER-LAW EXPONENT OF PSD(K) VS K CORRESPONDING
  ; TO THE SELECTED FIELD COMPONENT. THIS ROUTINE USES THE CONTONUOS WAVELET TRANSFORM TO ANALYZE THE FIELD
  ; COMPONENT. FOR MORE DETAILS ON THE CALCULATIONS SEE HEWETT ET. AL. (2008)
  ;
  dir1 ='/code/'

  ; IMPORT PARAMETERS
  ini_params, dir1+'params.json', arr=params
  
  ; READ IN SOME GLOBAL PARAMETERS
  ini_date = urldate(params.algorithm.start_time)   ; INITIAL DATE
  fin_date = urldate(params.algorithm.end_time)     ; FINAL DATE
  provenance = params.algorithm.provenance          ; PROVENANCE NAME FOR WRITING INTO DB
  comp = params.algorithm.comp               ;
  ; PARAMS FOR SPECIFIC PROPERTY EXTRACTION
  range = params.algorithm.range                    ; RANGE IN K-VALUES TO PERFORM THE LINEAR FIT OF DATA
  nscales = params.algorithm.nscales                ; NUMBER OF SCALES TO ANALYZE THE IMAGE AT

  ; CREATING A JSON STRUCTURE FOR REPORTING RESULTS
  pe_result = {algorithm_name: 'Alpha_exponent_cwt',algorithm_parameters: {alpha: 0.0,sigma: 0.0, fit_r: 0.0, $
    B_comp: comp,linfit_range: string(1./range[0],format='(I2)')+'-'+string(1./range[1],format='(I2)')+' Mm'} }
  post_data = {long_hg: 0., time_start: '', lat_hg: 0., long_carr: 0., nar: 0, data: pe_result}

  ; OBTAIN FILES URLS AND METADATA FROM SERVER USING API SERVER. RESULT IS A STRUCTURE (DATA_STR) WITH FILES AT THE
  ; SET CADENCE. /MIDNIGHT CORRESPOND TO ONE A DAY AT TIME 00:00. NONE FOR ALL THE FILES BETWEEN THE SELECTED DATES
  retrieve_data, ini_date, fin_date, data_str, sz, 'none'

  ; LOOP OVER ALL TIMES+SHARP REGIONS IN RANGE
  for i=0, sz(0)-1 do begin
    ;
    if (comp ne 'Btot') then begin
    
      get_files, data_str[i], comp, files_out, f_meta, t_obs
    
      ; OPENING FITS FILES
      read_sdo, files_out[0], ind, unc_img, /silent
    
    endif
    
    if (comp eq 'Btot') then begin
      
      comp = ['Br','Bp','Bt',comp]
      
      get_files, data_str[i], comp, files_out, f_meta, t_obs

      ; OPENING FITS FILES
      read_sdo, files_out[0], ind, unc_img_1, /silent
      read_sdo, files_out[1], ind, unc_img_2, /silent
      read_sdo, files_out[2], ind, unc_img_3, /silent
      unc_img = (unc_img_1^2. + unc_img_2^2. + unc_img_3^2.)^0.5
      
      comp = 'Btot'
      
    endif
    
    ; CORRECTING IMAGES FOR ANY ARTIFACTS (E.G. LIMB PRESENCE)
    image_check_correct, unc_img, ind, f_meta, c_img, pos_out, new_ind, lon, lat
    
    ; CALCULATING ALPHA
    alpha_exp_cwt, c_img, new_ind, nscales, range, linfit_params
    
    post_data.data.algorithm_parameters.alpha = linfit_params[0]
    post_data.data.algorithm_parameters.sigma = linfit_params[1]
    post_data.data.algorithm_parameters.fit_r = linfit_params[2]

    ; INSERTING THE AR INFORMATION INTO THE JSON STR
    make_json4db, pe_result, pos_out[0], pos_out[1], t_obs, pos_out[2], fix(f_meta.noaa_ars), json_data

    ; SEND THE PROPERTY TO THE DATA BASE
    push_data2db, provenance, json_data

    ; DELETING FILES
    if (comp ne 'Btot') then spawn, 'rm -f '+files_out[0]
    if (comp eq 'Btot') then begin
      spawn, 'rm -f '+files_out[0]
      spawn, 'rm -f '+files_out[1]
      spawn, 'rm -f '+files_out[2]
    endif
    ; stop
  endfor
  ;
END