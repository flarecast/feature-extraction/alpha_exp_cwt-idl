;+
; NAME:
;   do_cwt2d
; PURPOSE:
;   Perform a 2-D continuous wavelet decomposition on an image using the 
;		Mexican Hat wavelet at a series of scales.
;
; CALLING SEQUENCE:
;   do_cwt2d, image, scales, [scale_series=scale_series, n_scales=n_scales, wav_norms=wav_norms, $
;							range=range, loud=loud, [err_status=err_status, err_message=err_message]]
;
; EXAMPLES:
;	do_cwt2d( image, [1.0, 1.2, 1.4, 1.6, 1.8, 2.0], wav_norms=wav_norms, err_status=err_status, err_message=err_message, /loud)
;	do_cwt2d( image, [1.0, 0.2, 2.0], /range, scale_series=scale_series, n_scales=n_scales, wav_norms=wav_norms, /loud)
;
;
; INPUTS:
;   image -- A 2-D Image to Filter
;	scales -- One of two things (depending on the status of the range keyword.
;				If Range is not set, scales is a list of scales to run the 
;					wavelet at
;				If Range is set, scales should be a three element vector in the
;					form: scales=[start_scale, step, end_scale]
;
; KEYWORD PARAMETERS:
;	range -- when set, scales is a range of scales to be calculated by the software
;	loud -- verbose output
;
; OUTPUTS: 
;	scale_series -- the resulting list of scales that were analyzed
;	n_scales -- the number of scales that were analyzed
;	wav_norms -- a list of the L2 norms of the masks
;
; ERROR PARAMETERS:
;	err_status -- 	0 means no error
;					1 means fatal error
;					2 means warning
;	err_message -- text describing the error if reason known
;
; RETURN VALUE:
;   result -- a 3d matrix of the resulting scales
;
; RESTRICTIONS:
;	scales should be between 1 and N for an NxN image.  The high bound should also be
;		limited by the critical scale, but we dont know how to calculate that yet.
;
; MODIFICATION HISTORY:
;
;       03-Jul-2005, Russ Hewett (rhewett2@uiuc.edu)
;			-Written (finalized)
;			07-Jan-2016, Jordan Guerra (jordan.guerra@gmail.com)
;			- Modified to set the size of the image to a even number in each direction
;
;-

function do_cwt2d, 	image, scales, $ ;required parameters
					scale_series=scale_series, n_scales=n_scales, wav_norms=wav_norms, $ ;optional return parameters
		 			range=range, loud=loud, $ ;switches
					err_status=err_status, err_message=err_message ; error outputs

	err_status=0
					
	; if we are in range mode, prepare the list of scales, otherwise, we already have it
	if keyword_set(range) then begin
		
		if n_elements(scales) ne 3 then begin
			err_Status=1
			err_message='ERROR: Not enough elements for range input.  Format: scales=[start scale, step, end_Scale].'
		endif
		
		if scales[0] ge scales[2] then begin
			err_Status=1
			err_message='ERROR: Start scale cannot be higher than end scale.'			
		endif	
	
		;calculate the total number of scales
		scale_step = scales[1]
		n_steps = scales[2]-scales[0]
		steps_per_unit = 1./scale_step
		n_steps = n_steps * steps_per_unit + 1
		
		;set the starting scale
		scale=scales[0]
	end else begin
	
		n_steps = n_elements(scales)
		
		if n_steps lt 1 then begin
			err_Status=1
			err_message='ERROR: Not enough elements for range input.  Format: scales=[start scale, step, end_Scale].'			
		endif
		
		;set the starting scale
		scale=scales[0]
	endelse
	
	; First check that image size is evenly numbered in each direction - works better for defining the frequecies
	sz=size(image,/dim)
	nx = sz(0)
	ny = sz(1)
	
	for i=0,1 do begin
	  fnum = round(sz[i])
	  num=2*(fnum/2)
	  if (num eq fnum) then sz[i]=sz[i]
	  if (num ne fnum) then sz[i]=sz[i]-1
	endfor
	;print, sz
	; Triming the image
	image = image[0:sz[0]-1,0:sz[1]-1]
  
	;create the output matrices
	sz = size(image,/dim)
	;nn = min(sz)
	result = fltarr(sz[0],sz[1],n_steps)
	
	;scale_series is the list of scales
	scale_series = fltarr(n_steps)

	;the list of L2 norms	
	wav_norms = fltarr(n_steps)
	
	
	for i=0,n_steps-1 do begin
		
		;select the next scale from the list if we are not in range mode
		if not keyword_set(range) then scale = scales[i]
		
		;print some status information, only really useful if you want to track progress
		if keyword_set(loud) then print, i, scale, n_steps
		
		;get the resulting coefficient at scale scale and save the wav_norm and scale
		temp_result = cwt2d(image, scale, wav_norm=wav_norm)
		wav_norms[i]=wav_norm
		scale_series[i]=scale
		
		;copy over the coefficient
		result[*,*,i]=temp_result	
		
		; increment the scale for range mode
		if keyword_set(range) then scale = scale+scale_step
			
	endfor
	
	;set the total number of scales for the return value
	n_scales = n_steps
	
	; catch any errors to try and not break the program
	error_handler:
		if err_status eq 1 then begin
			;do any error handling
			result=-1
		endif
	
	
	return, result
	
end
