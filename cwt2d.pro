;+
; NAME:
;   cwt2d
; PURPOSE:
;   Perform a 2-D continuous wavelet decomposition on an image using the
;   Mexican Hat wavelet.  Adapted from routines in the
;   YAWTB (http://www.fyma.ucl.ac.be/projects/yawtb).
;
; CALLING SEQUENCE:
;   cwt2d, image, scale, [order=order, sigma=sigma, wav_norm=wav_norm, [err_status=err_status, err_message=err_message]]
;
; INPUTS:
;   image -- A 2-D Image to Filter
; scale -- The scale at which the image should be analyzed.  Should be
;       between 1 and N for an NxN image.  Can be any real value
;       within those bounds, though in practice scales above 40
;       (for a 300x300 image) don't tend to work.  This varies
;       depending on image size.
;
; KEYWORD PARAMETERS:
;   order -- Left over from YAWTB (someone can fill in the purpose of this
; sigma -- Left over from YAWTB (someone can fill in the purpose of this
;
; OUTPUTS:
; wav_norm -- The L2 norm of the mask generated for this scale
;
; ERROR PARAMETERS:
; err_status --   0 means no error
;         1 means fatal error
;         2 means warning
; err_message -- text describing the error if reason known
;
; RETURN VALUE:
;   coeff -- The resulting wavelet coefficient.
;
; RESTRICTIONS:
; scale should be between 1 and N.  See above.
;
; MODIFICATION HISTORY:
;
;       02-Jul-2005, Russ Hewett (rhewett2@uiuc.edu)
;     -Written (finalized)
;       06-Jan-2016, Jordan Guerra (jordan.guerra@gmail.com)
;     -Changed from using a squared size mask to a maks with the same
;     dimensions as the input image
;-

function cwt2d,   image, scale, $   ; required parameters
  order=order, sigma=sigma, $ ; optional input parameters
  wav_norm=wav_norm, $ ; optional output parameters
  err_status=err_status, err_message=err_message ; error outputs
  
  ; default the order and the sigma.  this is the default setting from YAWTB
  if not keyword_set(order) then order=2
  if not keyword_set(sigma) then sigma=1
  
  ; default err_status
  err_status=0
  
  ; n is the length along the longest edge of the image
  sz=size(image,/dim)
  nx = sz(0)
  ny = sz(1)
  n =  min([nx,ny])
  coeff=fltarr(nx,ny)
  
  ; check the range of scale
  if (scale lt 1.) then begin
    err_status=2
    err_message='WARNING: Scale may be too low.  Scale should be greater than or equal to 1.'
    ;goto error_handler
  endif
  
  SOMEVALUE = n+1. ; update this to be the value of the critical scale if we determine a metric
  ;  for calculating said critical scale
  
  if (scale gt SOMEVALUE) then begin
    err_status=2
    err_message='WARNING: Scale may be too high.  Scale should be less than size of image (and less than some critical scale that we have not calculated.'
    ;goto error_handler
  endif
  
  
  
  ; this next section is the complicated IDL way of duplicating 3 lines of matlab code.
  
  ; first, establish the step size as the angular size of each pixel
  stepx = 2. * !Pi / nx
  stepy = 2. * !Pi / ny
  
  ; make a vector of length n containing a saw tooth.  this is the basis of the y k-vector
  ky_base = fltarr(ny)
  ky_base = [findgen((ny/2)), (-1*reverse(findgen((ny/2))+1))] ;
  ky_base = stepy * ky_base
  
  ; create the y k-vector and replicate the base throughout the matrix
  ky = fltarr(nx,ny)
  for i=0,nx-1 do ky[i,*] = ky_base
  
  ; do the same for the kx vector 
  kx_base = fltarr(nx)
  kx_base = [findgen((nx/2)), (-1*reverse(findgen((nx/2))+1))] ;
  kx_base = stepx * kx_base
  
  ; create the y k-vector and replicate the base throughout the matrix
  kx = fltarr(nx,ny)
  for i=0,ny-1 do kx[*,i] = kx_base
  
  ; then, to get the y k-vector oriented correctly it must be flipped
  ky = reverse(ky,2)
  
  ; carefully converted from matlab because of row vs col majorness (more IDL screwyness)
  dkxdky = abs( (kx(1,0) - kx(0,0)) * (ky(0,1) - ky(0,0)) )
  
  
  ; dilate each k-matrix by the supplied scale.  if this were a directional transform, this is where
  ; you would multiply by sines and cosines
  kx = kx * scale
  ky = ky * scale
  
  ; calculate the Mexican Hat wavelet in two dimmensions
  wavelet_coeff = 2 * !Pi * (kx^2. + ky^2.)^(order/2.) * exp(-((sigma * kx)^2. + (sigma * ky)^2.)/2.)
  ;help, wavelet_coeff
  
  ; assume an L2 norm
  norm=1
  mask = scale^norm * wavelet_coeff
  ;plot_image, mask
  
  ; take the fft of the image and multiply it with the the coefficient
  image_fft = fft(image,/double)
  ;plot_image, alog10(abs(image_fft)^2.0)
  ;help, image_fft
  coeff_fft = mask * image_fft
  ;help, coeff_fft
  ;plot_image, alog10(abs(coeff_fft)^2.0)
  ; get the inverse transform to obtain the wavelet coefficient in scale space
  coeff = fft(coeff_fft,/double,/inverse)
  ;plot_image, coeff
  
  ; calculate L2 norm of the wavelet mask, this is used to help determine valid scales
  wav_norm = (total(abs(mask)^2)*dkxdky)^0.5/(2*!Pi);
  ;help, wav_norm
  
  ; catch any errors to try and not break the program
  error_handler:
  if err_status eq 1 then begin
    ;do any error handling
    coeff=fltarr(nx,ny)
  endif
  
  return, coeff
  
end