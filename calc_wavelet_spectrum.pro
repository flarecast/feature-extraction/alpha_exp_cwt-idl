FUNCTION CALC_WAVELET_SPECTRUM, img, n_scales, res, runit
  ;
  ; THIS ROUTINE CALCULATES THE ENERGY SPECTRUM OF THE IMAGE BY USING A WAVELET TRANSFORM METHOD
  ;
  sz=size(img,/dim)
  ;
  pi=4.0*atan(1.0)
  ;
  if (runit eq 'arcsec') then delta = (res*pi*1.49598e+11)/(648000.0) ;meter
  if (runit eq 'degree') then delta = (sin((res*2.0*pi)/360.)*6.96e+8) ;meter ;3.65E+5;
  area=delta*delta
  ;
  fc=0.278 ; CRITICAL FREQUENCY
  ; SPECTRUM AND K-VALUES
  spectrum=dblarr(n_scales)
  kval=dblarr(n_scales)
  ;
  k=findgen(n_scales)
  a=(1.3^(0.2*k))-1+0.1;k+1
  ;
  result = do_cwt2d(img,a,wav_norms=wav_norms)
  result = result/n_elements(img)
  result = abs(result)^2.
  ;
  spectrum = total(result,1)
  spectrum = total(spectrum,1)
  spectrum = spectrum/(fc*wav_norms)
  ;spectrum = spectrum/n_elements(img)
  ;
  kval=fc/(a*delta)
  ;
  return, [[spectrum],[kval]]
  ;
END