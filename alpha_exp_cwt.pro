PRO ALPHA_EXP_CWT, img, ind, nscales, range, linfit_params

; CALCULATING 1D POWER SPECTRAL DENSITY (PSD)
res = ind.cdelt1
psd_data=calc_wavelet_spectrum(img,nscales,res,'degree')
;
psd=reform(psd_data[*,0])
kmod=reform(psd_data[*,1])

; CALCULATING SLOPES - LINEAR FIT
linfit_psd, psd, kmod, range, linfit_params

END